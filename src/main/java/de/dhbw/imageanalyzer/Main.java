package de.dhbw.imageanalyzer;

import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.data.ImageAnalysis;
import de.dhbw.imageanalyzer.data.Line;
import de.dhbw.imageanalyzer.data.TwoImages;
import de.dhbw.imageanalyzer.processing.Task;
import de.dhbw.imageanalyzer.processing.TaskScheduler;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.LinearGrayScaleSpreadingTask;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.MergeSimilarImagesTask;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.SimpleGrayScaleTask;
import de.dhbw.imageanalyzer.util.Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class Main {

    public static final String IMAGE_0 = "/Image3_90deg.bmp";
    public static final String IMAGE_1 = "/Image1.bmp";
    public static final String IMAGE_2 = "/Image2.bmp";
    public static final String IMAGE_3 = "/Image3.bmp";
    public static final String IMAGE_4 = "/DOC_IMG1.jpg";
    //    public static final String IMAGE_5 = "/DOC_IMG2.jpg";
//    public static final String IMAGE_6 = "/DOC_IMG3.jpg";
//    public static final String IMAGE_7 = "/DOC_IMG4.jpg";
//    public static final String IMAGE_8 = "/DOC_IMG5.jpg";
    public static final String IMAGE_9 = "/DOC_IMG3_skaliert.png";
    public static final String IMAGE_10 = "/DOC_IMG4_skaliert.png";

    public static void main(String[] args) {
        try {
            ImageAnalyzer.initialize();
            List<BufferedImage> l = getAllImages();
            MergeSimilarImagesTask todo = new MergeSimilarImagesTask();
            Image one = new Image(l.get(0));
            Image two = new Image(l.get(3));
            TwoImages twoImages = new TwoImages(one, two);
            todo.setItem(twoImages);
            Task task = new Task<>(todo, (result, throwable) -> {
                if (throwable == null) {
                } else
                    throwable.printStackTrace();
                TaskScheduler.getInstance().shutDown();
            });
            TaskScheduler.getInstance().processAsync(task);
//            analyzeLines(l);
            //doTestingStuff(l);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void analyzeLines(List<BufferedImage> l) {
        for (int i = 0; i < l.size(); i++) {
            System.out.println("\n<<< IMAGE " + i + " >>>\n");
            ImageAnalysis analysis = ImageAnalyzer.analyzeImage(new Image(Utils.getData(l.get(i))));
            if (analysis.getDebugAnalysisInfo() != null) {
                System.out.println("Total time: " + (analysis.getDebugAnalysisInfo().getTotalTime() / 1000.0));
                System.out.println("Preparing time: " + (analysis.getDebugAnalysisInfo().getPreparingTime() / 1000.0));
                System.out.println("Analyzing time: " + (analysis.getDebugAnalysisInfo().getAnalyzingTime() / 1000.0));
            }
            System.out.println("\n<<< LINES >>>\n");
            for (Line line : analysis.getLines()) {
                System.out.println("Found " + line.getAlignment().name() + " line with length of: " + line.getLength());
                System.out.println("With median width of: " + line.getMeanWidth());
                System.out.println("Total pixel count of: " + line.getTotalCount());
            }
            System.out.println("\n<<< HOLES >>>\n");
            for (Line line : analysis.getHoles()) {
                System.out.println("Found " + line.getAlignment().name() + " hole with length of: " + line.getLength());
                System.out.println("With median width of: " + line.getMeanWidth());
                System.out.println("Total pixel count of: " + line.getTotalCount());
            }
            try {
                File f = new File(System.getProperty("user.dir") + "/results/");
                if (!f.exists() || !f.isDirectory()) {
                    f.mkdirs();
                }
                ImageIO.write(Utils.visualizeResults(analysis), "PNG",
                        new File(System.getProperty("user.dir") + "/results/img_ild" + i + ".png"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(">>> FINISHED img" + i + "!");
        }
        ImageAnalyzer.stop();
    }

    public static void doTestingStuff(List<BufferedImage> l) throws Exception {
        int testpic = 2;
        for (int i = testpic; i <= testpic/*l.size()*/; i++) {
            SimpleGrayScaleTask sgst = new SimpleGrayScaleTask();
            sgst.setItem(new Image(Utils.getData(l.get(i))));
            LinearGrayScaleSpreadingTask lgsst = new LinearGrayScaleSpreadingTask();
            lgsst.setItem(sgst.call());
            lgsst.setLowCut(0.1);
            lgsst.setHighCut(0.7);
            lgsst.call();
            System.out.println("Finished img" + i + "!");
        }
        ImageAnalyzer.stop();
    }


    public static List<BufferedImage> getAllImages() {
        LinkedList<BufferedImage> l = new LinkedList<>();
        for (Field field : Main.class.getDeclaredFields()) {
            int modifiers = field.getModifiers();
            if (Modifier.isFinal(modifiers) && Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers) && field.getType().equals(String.class)) {
                try {
                    l.add(ImageIO.read(Main.class.getResourceAsStream((String) field.get(null))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println(l.size() + " images loaded.");
        return l;
    }
}
