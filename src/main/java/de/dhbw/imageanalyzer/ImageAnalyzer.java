package de.dhbw.imageanalyzer;

import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.data.ImageAnalysis;
import de.dhbw.imageanalyzer.processing.Task;
import de.dhbw.imageanalyzer.processing.TaskFinishedListener;
import de.dhbw.imageanalyzer.processing.TaskScheduler;
import de.dhbw.imageanalyzer.processing.tasks.highlevel.AnalyzeImageTask;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.opencv.core.Core;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Logger;

/**
 * Use this class to access functionality of this libary!
 * If you try to access other components without calling this class it may be cause errors!
 * If you need to access other components call at least ImageAnalyzer.initialize()!
 *
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class ImageAnalyzer {

    private static final Logger LOG = Logger.getLogger(ImageAnalyzer.class.getName());
    private static final String SYSTEM_LIBRARY_KEY = "java.library.path";
    private static boolean initialized = false;

    /**
     * Initializes the library (loads native library of openCV)
     * by automatically detecting the OS and using build in
     * openCV libraries.
     */
    public static synchronized void initialize() {
        if (!initialized) {
            initialized = true;
            String libraryPath = "";
            String libraryName = Core.NATIVE_LIBRARY_NAME;
            String suffix = "";
            String architecture = System.getProperty("os.arch").contains("64") ? "x64" : "x32";
            if (SystemUtils.IS_OS_WINDOWS) {
                libraryPath = "/lib/opencv/windows/" + architecture + "/";
                suffix = ".dll";
            } else if (SystemUtils.IS_OS_LINUX) {
                libraryPath = "/lib/opencv/linux/" + architecture + "/";
            }
            try {
                String output = "";
                try {
                    InputStream in = ImageAnalyzer.class.getResourceAsStream(libraryPath + libraryName + suffix);
                    File fileOut = new File(System.getProperty("java.io.tmpdir") + "/" + libraryPath + libraryName + suffix);
                    OutputStream out = FileUtils.openOutputStream(fileOut);
                    IOUtils.copy(in, out);
                    output = fileOut.getParent();
                    in.close();
                    out.close();
                } catch (Exception e) {
                    LOG.warning("Couldn't copy given openCV library to tmp location to load!");
                }
                String baseLibrary = System.getProperty(SYSTEM_LIBRARY_KEY);
                if (baseLibrary == null) baseLibrary = "";
                System.setProperty(SYSTEM_LIBRARY_KEY, baseLibrary + ";" + output);
                refreshPaths();
                System.loadLibrary(libraryName);
            } catch (Throwable e) {
                LOG.warning("Failed to load openCV library! Either your OS couldn't be detected and the library couldn't be loaded out of this libraries sources or you have to install openCV first on your device!");
            }
        }
    }

    private static void refreshPaths() {
        try {
            ClassLoader classLoader = ImageAnalyzer.class.getClassLoader();
            Method method = ClassLoader.class.getDeclaredMethod("initializePath", new Class[]{String.class});
            method.setAccessible(true);
            String[] paths = (String[]) method.invoke(null, new Object[]{SYSTEM_LIBRARY_KEY});
            Field pathsField = ClassLoader.class.getDeclaredField("usr_paths");
            pathsField.setAccessible(true);
            pathsField.set(null, paths);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    public static void analyzeImage(Image image, TaskFinishedListener<ImageAnalysis>... listeners) {
        initialize();
        if (!TaskScheduler.getInstance().isRunning()) {
            TaskScheduler.getInstance().start();
        }
        AnalyzeImageTask ait = new AnalyzeImageTask();
        ait.setItem(image);
        TaskScheduler.getInstance().processAsync(new Task(ait, listeners));
    }

    public static ImageAnalysis analyzeImage(Image image) {
        initialize();
        AnalyzeImageTask ait = new AnalyzeImageTask();
        ait.setItem(image);
        return TaskScheduler.getInstance().process(ait);
    }

    public static void stop() {
        if (TaskScheduler.getInstance().isRunning()) {
            TaskScheduler.getInstance().shutDown();
        }
    }
}
