package de.dhbw.imageanalyzer.processing;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public interface TaskFinishedListener<RETURNS> {

    public void finished(RETURNS item, Throwable t);
}
