package de.dhbw.imageanalyzer.processing;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class TaskScheduler {

    private static TaskScheduler instance;

    private int maxThreads = 2;
    private ExecutorService service = Executors.newFixedThreadPool(maxThreads);
    private boolean running = false;

    private TaskScheduler() {}

    public boolean isRunning() {
        return running;
    }

    public <TAKES, RETURNS> void processAsync(Task<TAKES, RETURNS> task) {
        try {
            service.execute(task);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public <TAKES, RETURNS> RETURNS process(ACallable<TAKES, RETURNS> task) {
        try {
            return task.call();
        } catch (Throwable e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void shutDown() {
        if (service != null) {
            service.shutdown();
        }
        running = false;
    }

    public void start() {
        if (service != null) {
            service.shutdown();
        }
        service = Executors.newFixedThreadPool(maxThreads);
        running = true;
    }

    public void setMaxThreads(int threads) {
        this.maxThreads = threads;
        shutDown();
        start();
    }

    public int getMaxThreads() {
        return maxThreads;
    }

    public static TaskScheduler getInstance() {
        if (instance == null) {
            synchronized (TaskScheduler.class) {
                if (instance == null) {
                    instance = new TaskScheduler();
                }
            }
        }
        return instance;
    }

}
