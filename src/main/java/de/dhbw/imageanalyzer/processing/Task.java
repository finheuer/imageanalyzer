package de.dhbw.imageanalyzer.processing;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class Task<TAKES, RETURNS> implements Runnable {

    private final TaskFinishedListener<RETURNS>[] listeners;
    private final ACallable<TAKES, RETURNS> callable;

    public Task(ACallable<TAKES, RETURNS> callable, TaskFinishedListener<RETURNS>... listeners) {
        this.listeners = listeners;
        this.callable = callable;
    }

    @Override
    public void run() {
        RETURNS returnValue = null;
        Throwable t = null;
        try {
            returnValue = execute();
        } catch (Throwable th) {
            t = th;
        }
        if (listeners != null) {
            for (TaskFinishedListener<RETURNS> listener : listeners) {
                listener.finished(returnValue, t);
            }
        }
    }

    public RETURNS execute() throws Exception {
        return callable.call();
    }
}
