package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.MatchingResult;
import de.dhbw.imageanalyzer.data.TwoImages;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.util.Utils;
import org.opencv.core.*;
import org.opencv.features2d.*;

import java.util.LinkedList;
import java.util.List;

public class SimilarityTask extends ACallable<TwoImages, MatchingResult> {

    //BFMatcher.create(Core.NORM_HAMMING, true) k=1!
    private final static DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
    private final double nnMatchRatio = 0.8;

    @Override
    public MatchingResult call() throws Exception {
        ORB orb = ORB.create();
        Mat img1 = Utils.imageToMat(getItem().getImage1());
        Mat img2 = Utils.imageToMat(getItem().getImage2());
        MatOfKeyPoint matOfKeyPoint1 = new MatOfKeyPoint();
        MatOfKeyPoint matOfKeyPoint2 = new MatOfKeyPoint();
        Mat descriptors1 = new Mat();
        Mat descriptors2 = new Mat();
        orb.detectAndCompute(img1, new Mat(), matOfKeyPoint1, descriptors1);
        orb.detectAndCompute(img2, new Mat(), matOfKeyPoint2, descriptors2);
        List<MatOfDMatch> matches = new LinkedList<>();
        List<KeyPoint> matched1 = new LinkedList<>();
        List<KeyPoint> matched2 = new LinkedList<>();
        matcher.knnMatch(descriptors1, descriptors2, matches, 2);
        for (int i = 0; i < matches.size(); i++) {
            DMatch[] match = matches.get(i).toArray();
            if (match[0].distance < nnMatchRatio * match[1].distance) {
                matched1.add(matOfKeyPoint1.toArray()[match[0].queryIdx]);
                matched2.add(matOfKeyPoint2.toArray()[match[0].trainIdx]);
            }
        }
        return new MatchingResult(descriptors1.height(), descriptors2.height(), matches.size(), matched1.size());
    }
}