package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;

/**
 * So far, only possible for one specific color! > Color check
 *
 * @author Ulrike Leipscher <ulrike.leipscher@dlr.de>.
 */
public class DilationTask extends ACallable<Image, Image> {
    /* mask contains a pattern in any NxM Dimensions:
       1 equals white pixel
       0 equals black
       -1 marks points of interest

       examples:
       1    1   1
       1    -1  1
       1    1   1

       0    1   0
       1    -1  1
       0    1   0

       1    1   -1  1   1

       -1   0
       1    1

       If image pixel at point of interest is white, all pixels around this one that match the 1`s in the mask will converted to white
     */
    private int[][] mask;
    private int width;
    private int height;
    private int xPOI;
    private int yPOI;
    private Color replace = Color.WHITE;
    private Color check = Color.WHITE;

    public DilationTask() {
    }

    public DilationTask(Color replace) {
        this.replace = replace;
    }

    public void setCheck(Color check) {
        this.check = check;
    }

    public void setMask(int[][] mask) {
        this.mask = mask;
    }

    public void setReplace(Color replace) {
        this.replace = replace;
    }

    /**
     * !!! overwrites data of input image object
     *
     * @return input image with new color data
     * @throws Exception
     */
    @Override
    public Image call() throws Exception {
        analyseMask();
        Color[][] imgData = getItem().getData();
        Color[][] newImgData = getItem().clone().getData();
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                if (imgData[x][y].equals(check)) {
                    boolean fitsMask = true;
                    for (int mx = 0; mx < width; mx++) {
                        for (int my = 0; my < height; my++) {
                            if (mask[mx][my] == -1) {
                                int curX = x + my - yPOI;
                                int curY = y + mx - xPOI;
                                if (!(isInBounds(curX, curY) && imgData[curX][curY].equals(check))) {
                                    fitsMask = false;
                                    break;
                                }
                            }
                        }
                        if (!fitsMask) break;
                    }
                    if (fitsMask) {
                        for (int mx = 0; mx < width; mx++) {
                            for (int my = 0; my < height; my++) {
                                if (mask[mx][my] == 1) {
                                    int curX = x + my - yPOI;
                                    int curY = y + mx - xPOI;
                                    if (isInBounds(curX, curY)) {
                                        newImgData[curX][curY] = replace;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        getItem().setData(newImgData);
        return getItem();
    }

    private boolean isInBounds(int x, int y) {
        return (x >= 0 && x < getItem().getWidth())
                && (y >= 0 && y < getItem().getHeight());
    }

    private void analyseMask() {
        width = mask.length;
        height = mask[0].length;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (mask[x][y] == -1) {
                    xPOI = x;
                    yPOI = y;
                    break;
                }
            }
        }
    }
}
