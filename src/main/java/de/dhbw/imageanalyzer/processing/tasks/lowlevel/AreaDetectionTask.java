package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.data.Pixel;
import de.dhbw.imageanalyzer.processing.ACallable;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

/**
 * @author Ulrike Leipscher <ulrike.leipscher@dlr.de>.
 */
public class AreaDetectionTask extends ACallable<Image, Image> {

    private int maxPixel = 300;
    private Color replace = Color.BLACK;

    private List<SortedSet<Integer>> relationships;
    private int[][] labels;

    public AreaDetectionTask() {
    }

    public AreaDetectionTask(int maxPixel, Color replace) {
        this.maxPixel = maxPixel;
        this.replace = replace;
    }

    public void setMaxPixel(int maxPixel) {
        this.maxPixel = maxPixel;
    }

    public int getMaxPixel() {
        return maxPixel;
    }

    /**
     * !!! overwrites input image
     *
     * @return input image with new color data
     * @throws Exception
     */
    @Override
    public Image call() throws Exception {
        Color[][] imgData = getItem().getData();
        relationships = new ArrayList<>();
        labels = new int[imgData.length][imgData[0].length];
        int curLabel = 0;
        SortedSet<Integer> curRelations = new ConcurrentSkipListSet<>();
        Set<Pixel> whitePixels = new HashSet<>();
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                if (imgData[x][y].equals(Color.WHITE)) {
                    whitePixels.add(new Pixel(x, y));
                    if (y > 0 && labels[x][y - 1] > 0) {
                        labels[x][y] = labels[x][y - 1];
                        curRelations = checkRelations(curRelations, x, y);
                    } else if (x > 0 && labels[x - 1][y] > 0) {
                        labels[x][y] = labels[x - 1][y];
                    } else {
                        curLabel++;
                        labels[x][y] = curLabel;
                    }
                } else {
                    labels[x][y] = 0;
                }
            }
        }
        reorderRelations(curLabel);
        Map<Integer, Integer> counts = mergeEquallyRelatedLabels();
        deleteSpots(imgData, counts, whitePixels);
        return getItem();
    }

    private SortedSet<Integer> checkRelations(SortedSet<Integer> curRelations, int x, int y) {
        if (x > 0 && labels[x - 1][y] > 0 && labels[x - 1][y] != labels[x][y - 1]) {
            if (!curRelations.isEmpty() && !(curRelations.contains(labels[x][y]) || curRelations.contains(labels[x - 1][y]))) {
                relationships.add(curRelations);
                curRelations = new ConcurrentSkipListSet<>();
            }
            curRelations.add(labels[x][y]);
            curRelations.add(labels[x - 1][y]);
        }
        return curRelations;
    }

    private void reorderRelations(int maxLabel) {
        for (int i = 1; i <= maxLabel; i++) {
            final int label = i;
            SortedSet<Integer> combined = new ConcurrentSkipListSet<>();
            List<SortedSet> tmp = relationships.stream().filter(s -> s.contains(label)).collect(Collectors.toList());
            if (tmp.isEmpty()) {
                continue;
            }
            tmp.forEach(combined::addAll);
            relationships.removeAll(tmp);
            relationships.add(combined);
        }
    }

    private Map<Integer, Integer> mergeEquallyRelatedLabels() {
        Map<Integer, Integer> pixelCounts = new HashMap<>();

        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                if (labels[x][y] > 0) {
                    final int label = labels[x][y];
                    SortedSet<Integer> relation = relationships.stream().filter(s -> s.contains(label)).findAny().orElse(null);
                    labels[x][y] = relation != null ? relation.first() : label;
                    pixelCounts.put(labels[x][y], pixelCounts.getOrDefault(labels[x][y], 0) + 1);
                }
            }
        }
        return pixelCounts;
    }


    private Color[][] deleteSpots(Color[][] imgData, Map<Integer, Integer> counts, Set<Pixel> whites) {
        whites.stream().filter(p -> counts.get(labels[p.getX()][p.getY()]) <= maxPixel)
                .forEach(p -> imgData[p.getX()][p.getY()] = replace); //new Color(labels[p.getX()][p.getY()] * 100 + 167117680));
//        whites.stream().filter(p -> counts.get(labels[p.getX()][p.getY()]) > maxPixel)
//                .forEach(p -> imgData[p.getX()][p.getY()] = Color.GREEN);
        return imgData;
    }


    private void printLabels(Map<Integer, Integer> counts) {
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                System.out.print(labels[x][y] + "\t");
            }
            System.out.println();
        }
        relationships.forEach(s -> System.out.print(s.first() + " "));
        System.out.println("Anzahl = " + relationships.size());
        System.out.println(counts);
    }
}
