package de.dhbw.imageanalyzer.processing.tasks.highlevel;

import de.dhbw.imageanalyzer.data.*;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.SimpleGrayScaleTask;
import de.dhbw.imageanalyzer.util.Utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class AnalyzeImageTask extends ACallable<Image, ImageAnalysis> {

    @Override
    public ImageAnalysis call() throws Exception {
        long preparingStart = System.currentTimeMillis();
        List<Image> images = new ArrayList<>();
        ImageAnalysis ia = new ImageAnalysis(getItem());

        // => gray scale image
        SimpleGrayScaleTask sgst = new SimpleGrayScaleTask();
        sgst.setItem(getItem());
        Image img = sgst.call();

        ImprovedPrepareImageTask pit = new ImprovedPrepareImageTask(Alignment.HORIZONTAL);
        pit.setItem(img);
        images.add(pit.call());
        //UIUtils.showImage(Utils.getImage(images[0]), "Horizontal Preparation");
        pit.setAlignment(Alignment.VERTICAL);
        images.add(pit.call());
        //UIUtils.showImage(Utils.getImage(images[1]), "Horizontal Preparation");
        PrepareHolesTask pht = new PrepareHolesTask();
        pht.setItem(img);
        Image img2 = pht.call();
//        DeleteLinesTask dlt = new DeleteLinesTask(images, null);
//        dlt.setItem(img2);
//        images.add(dlt.call());
        //UIUtils.showImage(Utils.getImage(images[2]), "Hole Preparation");
//        ia.setPreparedImage(images);
        long preparingStop = System.currentTimeMillis();

        long analyzeStart = System.currentTimeMillis();
        // for lines:
        LinkedList<Set<Pixel>> lines = new LinkedList<>();
        lines.addAll(Utils.getLinesInImage(images.get(0)));
        lines.addAll(Utils.getLinesInImage(images.get(1)));
        List<Line> analyzed = Utils.analyzeImage(lines);
        ia.getLines().addAll(analyzed.stream().filter(l -> l.getLength() > l.getWidthOfRect() * 4).collect(Collectors.toList()));

        // for Holes:
        DeleteLinesTask dlt = new DeleteLinesTask(null, ia.getLines());
        dlt.setItem(img2);
        images.add(dlt.call());
        //UIUtils.showImage(Utils.getImage(images[2]), "Hole Preparation");
        ia.setPreparedImage(images);

        List<Set<Pixel>> holes = Utils.getLinesInImage(images.get(2), Color.RED);
        analyzed = Utils.analyzeImage(holes);
        ia.getHoles().addAll(analyzed.stream().filter(l -> l.getLength() < l.getWidthOfRect() * 4).collect(Collectors.toList()));
        long analyzeStop = System.currentTimeMillis();

        long preparingTime = preparingStop - preparingStart;
        long analyzingTime = analyzeStop - analyzeStart;
        ia.setDebugAnalysisInfo(new DebugAnalysisInfo(preparingTime + analyzingTime, preparingTime, analyzingTime));
        return ia;
    }
}
