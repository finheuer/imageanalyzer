package de.dhbw.imageanalyzer.processing.tasks.highlevel;

import de.dhbw.imageanalyzer.data.Alignment;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.*;
import de.dhbw.imageanalyzer.util.FilterFactory;
import de.dhbw.imageanalyzer.util.MaskFactory;

/**
 * @author Stefan Limbach <stefan.limbach@dlr.de>.
 */
public class PrepareHolesTask extends ACallable<Image, Image> {

    @Override
    public Image call() throws Exception {
        getItem().changeContrast(120);

//        ImageFilteringTask ift = FilterFactory.createNxNFilter(5, 0.75).build();
        ImageFilteringTask ift = FilterFactory.createGaussFilter(5, false).build();
        ift.setItem(getItem());
        Image img = ift.call();
        ift = FilterFactory.createGradientBuilder(Alignment.NORTH).build();
        ift.setItem(img);
        Image img1 = ift.call();
        ift = FilterFactory.createGradientBuilder(Alignment.SOUTH).build();
        ift.setItem(img);
        img1.addTo(ift.call());
        ift = FilterFactory.createGradientBuilder(Alignment.EAST).build();
        ift.setItem(img);
        img1.addTo(ift.call());
        ift = FilterFactory.createGradientBuilder(Alignment.WEST).build();
        ift.setItem(img);
        img1.addTo(ift.call());
        ThresholdTask ttt = new ThresholdTask(img1.getMeanBrightness() * 3);
        ttt.setItem(img1);
        img = ttt.call();
        CorrectEdgesTask cet = new CorrectEdgesTask();
        cet.setItem(img);
        img = cet.call();

        DeleteSpotsTask dst = new DeleteSpotsTask();
        int pixelThreshold = getItem().getHeight() * getItem().getWidth() / 8000 ;
        dst.setPixelThreshold(pixelThreshold);
        dst.setItem(img);
        img = dst.call();

        DilationTask dt = new DilationTask();
        dt.setMask(MaskFactory.createSpotMask(7));
        dt.setItem(img);
        img = dt.call();
        ErosionTask et = new ErosionTask();
        et.setMask(MaskFactory.createMaskNxM(5, 5));
        et.setItem(img);
        img = et.call();

        dst.setItem(img);
        dst.setPixelThreshold(dst.getPixelThreshold() / 2);
        img = dst.call();

        dt.setMask(MaskFactory.createMaskNxM(3, 3));
        dt.setItem(img);
        img = dt.call();

        return img;
    }
}
