package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;

/**
 * So far, only for one specific color possible > check
 *
 * @author Ulrike Leipscher <ulrike.leipscher@dlr.de>.
 */
public class ErosionTask extends ACallable<Image, Image> {

    /* mask contains a pattern in any NxM Dimensions:
       1 equals white pixel
       0 equals black
       -1 marks the point of interest (only one per mask)

       examples:
       1    1   1
       1    -1  1
       1    1   1

       0    1   0
       1    -1  1
       0    1   0

       1    1   -1  1   1

       -1   0
       1    1

       If image area fits the 1`s in mask, the point of interest can stay white, else it is inverted to black
     */
    private int[][] mask;
    private int width;
    private int height;
    private int xPOI;
    private int yPOI;
    private Color replace = Color.BLACK;
    private Color check = Color.WHITE;

    public ErosionTask() {
    }

    public ErosionTask(Color replace) {
        this.replace = replace;
    }

    public void setMask(int[][] mask) {
        this.mask = mask;
    }

    public void setReplace(Color replace) {
        this.replace = replace;
    }

    public void setCheck(Color check) {
        this.check = check;
    }

    /**
     * !!! overwrites input data !!!
     *
     * @return input image with new color data
     * @throws Exception
     */
    @Override
    public Image call() throws Exception {
        analyseMask();
        Color[][] imgData = getItem().getData();
        Color[][] newImgData = new Color[getItem().getWidth()][getItem().getHeight()];
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                boolean fitsMask = true;
                for (int mx = 0; mx < width; mx++) {
                    for (int my = 0; my < height; my++) {
                        if (mask[mx][my] == 1) {
                            int curX = x + my - yPOI;
                            int curY = y + mx - xPOI;
                            if ((curX >= 0 && curX < getItem().getWidth())
                                    && (curY >= 0 && curY < getItem().getHeight())
                                    && !imgData[curX][curY].equals(check)) {
                                fitsMask = false;
                                break;
                            }
                        }
                    }
                    if (!fitsMask) break;
                }
                if (fitsMask) {
                    newImgData[x][y] = imgData[x][y];
                } else {
                    newImgData[x][y] = replace;
                }
            }
        }
        getItem().setData(newImgData);
        return getItem();
    }

    private void analyseMask() {
        width = mask.length;
        height = mask[0].length;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (mask[x][y] == -1) {
                    xPOI = x;
                    yPOI = y;
                    break;
                }
            }
        }
    }
}
