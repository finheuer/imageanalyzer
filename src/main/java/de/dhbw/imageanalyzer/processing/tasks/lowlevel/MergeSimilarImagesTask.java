package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.data.TwoImages;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.util.UIUtils;
import de.dhbw.imageanalyzer.util.Utils;
import org.opencv.core.*;
import org.opencv.features2d.BFMatcher;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.Features2d;
import org.opencv.features2d.ORB;

import java.util.LinkedList;
import java.util.List;

public class MergeSimilarImagesTask extends ACallable<TwoImages, Void> {

    private final static DescriptorMatcher matcher = BFMatcher.create(Core.NORM_HAMMING, true);
    private final double nnMatchRatio = 0.8;

    @Override
    public Void call() throws Exception {
        //-------------------------
        //Gray scale the image!
        SimpleGrayScaleTask grayScaleTask = new SimpleGrayScaleTask();
        grayScaleTask.setItem(getItem().getImage1());
        getItem().setImage1(grayScaleTask.call());
        grayScaleTask.setItem(getItem().getImage2());
        getItem().setImage2(grayScaleTask.call());
        //-------------------------
        //Detect key points and calculate descriptors
        ORB orb = ORB.create();
        Mat img1 = Utils.imageToMat(getItem().getImage1());
        Mat img2 = Utils.imageToMat(getItem().getImage2());
        MatOfKeyPoint matOfKeyPoint1 = new MatOfKeyPoint();
        MatOfKeyPoint matOfKeyPoint2 = new MatOfKeyPoint();
        Mat descriptors1 = new Mat();
        Mat descriptors2 = new Mat();
        orb.detectAndCompute(img1, new Mat(), matOfKeyPoint1, descriptors1);
        orb.detectAndCompute(img2, new Mat(), matOfKeyPoint2, descriptors2);
        List<MatOfDMatch> matches = new LinkedList<>();
        List<KeyPoint> matched1 = new LinkedList<>();
        List<KeyPoint> matched2 = new LinkedList<>();
        matcher.knnMatch(descriptors1, descriptors2, matches, 1);
        //-------------------------
        //TODO: Bildausschnitt berechnen (Offset und Rechteck), skalieren, rotieren -> transformieren

        //-------------------------
        //Visualize key point matches
        Mat out = new Mat();
        Features2d.drawMatches2(img1, matOfKeyPoint1, img2, matOfKeyPoint2, matches, out);
        UIUtils.showImage(Utils.matToBufferedImage(out), "OUT 3");
        return null;
    }
}
