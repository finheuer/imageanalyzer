package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.data.Pixel;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.util.Utils;

import java.util.HashSet;
import java.util.Set;

public class DeleteSpotsTask extends ACallable<Image, Image> {

    private int pixelThreshold = 300;
    private Color replace = Color.BLACK;
    private Color baseColor = Color.WHITE;
    private boolean replaceBelowThresh = true;

    public DeleteSpotsTask() {
    }

    public DeleteSpotsTask(int pixelThreshold, Color replace) {
        this.pixelThreshold = pixelThreshold;
        this.replace = replace;
    }

    public void setPixelThreshold(int pixelThreshold) {
        this.pixelThreshold = pixelThreshold;
    }

    public void setReplace(Color replace) {
        this.replace = replace;
    }

    public void setBaseColor(Color baseColor) {
        this.baseColor = baseColor;
    }

    public Color getBaseColor() {
        return baseColor;
    }

    public Color getReplace() {
        return replace;
    }

    public int getPixelThreshold() {
        return pixelThreshold;
    }


    public boolean isReplaceBelowThresh() {
        return replaceBelowThresh;
    }

    public void setReplaceBelowThresh(boolean replaceBelowThresh) {
        this.replaceBelowThresh = replaceBelowThresh;
    }

    /**
     * !!! overwrites input image object with new color data
     *
     * @return unput image with new color data
     * @throws Exception
     */
    @Override
    public Image call() throws Exception {
        Color[][] imgData = getItem().getData();
        Set<Pixel> checkedPixels = new HashSet<>();
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                if (imgData[x][y].equals(baseColor)) {
                    Set<Pixel> whites = Utils.countPixelsSearch(imgData, x, y, checkedPixels);
                    if (replaceBelowThresh ? whites.size() <= pixelThreshold : whites.size() >= pixelThreshold) {
                        for (Pixel p : whites) {
                            imgData[p.getX()][p.getY()] = replace;
                        }
                    }
                }
            }
        }
        return getItem();
    }
}
