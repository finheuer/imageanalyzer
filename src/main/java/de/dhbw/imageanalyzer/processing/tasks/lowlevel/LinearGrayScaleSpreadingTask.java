package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;

/**
 * @author Stefan Limbach <stefan.limbach@dlr.de>.
 */
public class LinearGrayScaleSpreadingTask extends ACallable<Image, Image> {

    private double lowCut = 0.2;
    private double highCut = 0.9;
    private double lowScale = 0.0;
    private double highScale = 1.0;

    public double getLowCut() {
        return lowCut;
    }
    public void setLowCut(double lowCut) {
        this.lowCut = lowCut;
    }

    public double getHighCut() {
        return highCut;
    }
    public void setHighCut(double highCut) {
        this.highCut = highCut;
    }

    public double getLowScale() {
        return lowScale;
    }
    public void setLowScale(double lowScale) {
        this.lowScale = lowScale;
    }

    public double getHighScale() {
        return highScale;
    }
    public void setHighScale(double highScale) {
        this.highScale = highScale;
    }

    @Override
    public Image call() throws Exception {
        Image img = getItem();

        Color[][] color = img.getData();

        for(Color[] lines : color)
        {
            for(Color pixel : lines)
            {
                int r = pixel.getRed();
                int g = pixel.getGreen();
                int b = pixel.getBlue();

                r = imageAdjust(r);
                g = imageAdjust(g);
                b = imageAdjust(b);

                pixel.setRed(r);
                pixel.setGreen(g);
                pixel.setBlue(b);
            }
        }

        img.setData(color);

        return img;
    }

    private int imageAdjust(double color)
    {
        return (int) (lowCut + (highCut - lowCut) * ((color - lowScale) / (highScale - lowScale)));
    }

}
