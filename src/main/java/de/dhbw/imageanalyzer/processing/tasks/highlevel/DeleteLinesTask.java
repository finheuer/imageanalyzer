package de.dhbw.imageanalyzer.processing.tasks.highlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.data.Line;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.DeleteSpotsTask;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.DilationTask;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.ErosionTask;
import de.dhbw.imageanalyzer.util.MaskFactory;

import java.util.List;

/**
 * @author Stefan Limbach <stefan.limbach@dlr.de>.
 */
public class DeleteLinesTask extends ACallable<Image, Image> {

    private List<Image> linesImages;
    private List<Line> lines;

    public DeleteLinesTask(List<Image> linesImages, List<Line> lines) {
        this.linesImages = linesImages;
        this.lines = lines;
    }

    public List<Image> getLinesImages() {
        return linesImages;
    }

    public void setLinesImages(List<Image> linesImages) {
        this.linesImages = linesImages;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    @Override
    public Image call() throws Exception {
        final Image img = getItem();

//        UIUtils.showImage(Utils.getImage(img), "Before substract");
        if (linesImages != null) {
            for (Image image : linesImages) {
                img.subtract(image);
            }
        } else if (lines != null) {
            for (Line l : lines) {
                l.getPixels().forEach(p -> img.setRGBA(p.getX(), p.getY(), Color.BLACK.getRGBA()));
            }
        }

        ErosionTask et = new ErosionTask();
        et.setItem(img);
        et.setMask(MaskFactory.createSpotMask(3));
        Image result = et.call();

        DeleteSpotsTask dsp = new DeleteSpotsTask();
        dsp.setItem(result);
        dsp.setPixelThreshold(img.getHeight() * img.getWidth() / 10000);
        result = dsp.call();

        DilationTask dt = new DilationTask();
        dt.setItem(result);
        dt.setMask(MaskFactory.createSpotMask(3));
        result = dt.call();

//        UIUtils.showImage(Utils.getImage(result), "After substract");
        return result;
    }

}
