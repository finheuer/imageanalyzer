package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class CorrectEdgesTask extends ACallable<Image, Image> {

    private int size = 3;
    private Color fillColor = Color.BLACK;

    public CorrectEdgesTask() {
    }

    public CorrectEdgesTask(int size) {
        this.size = size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    /**
     * !!! overwrites input image
     *
     * @return input image with new color data
     * @throws Exception
     */
    @Override
    public Image call() throws Exception {
        Color[][] data = getItem().getData();
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int i = 0; i < size; i++) {
                if (i < getItem().getHeight()) {
                    data[x][i] = fillColor;
                }
            }
        }
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int i = getItem().getHeight() - 1; i >= getItem().getHeight() - size; i--) {
                if (i >= 0) {
                    data[x][i] = fillColor;
                }
            }
        }
        for (int y = 0; y < getItem().getHeight(); y++) {
            for (int i = 0; i < size; i++) {
                if (i < getItem().getWidth()) {
                    data[i][y] = fillColor;
                }
            }
        }
        for (int y = 0; y < getItem().getHeight(); y++) {
            for (int i = getItem().getWidth() - 1; i >= getItem().getWidth() - size; i--) {
                if (i >= 0) {
                    data[i][y] = fillColor;
                }
            }
        }
        return getItem();
    }
}
