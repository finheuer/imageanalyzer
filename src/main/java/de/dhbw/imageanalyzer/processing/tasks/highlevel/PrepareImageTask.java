package de.dhbw.imageanalyzer.processing.tasks.highlevel;

import de.dhbw.imageanalyzer.data.Alignment;
import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.*;
import de.dhbw.imageanalyzer.util.FilterFactory;
import de.dhbw.imageanalyzer.util.MaskFactory;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class PrepareImageTask extends ACallable<Image, Image> {

    private Alignment alignment;

    public PrepareImageTask(Alignment alignment) {
        this.alignment = alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    @Override
    public Image call() throws Exception {
        int maxPixel = getItem().getWidth() * getItem().getHeight() / 4000;
        // => smooth and darken
        ImageFilteringTask ift = FilterFactory.createNxNFilter(5, 0.75).build();
        ift.setItem(getItem());
        Image img = ift.call();
        // => detect upper/lower or left/right edges
        ift = FilterFactory.createEdgeFilter(alignment).build();
        ift.setItem(img);
        Image img1 = ift.call();
        ift = FilterFactory.createEdgeFilter(alignment).setTop(-1).setBottom(-1).build();
        ift.setItem(img);
        Image img2 = ift.call();
        // => add those two images
        img1.addTo(img2);
        // => apply threshold: bright pixels to white, dark ones to black
        ThresholdTask tt = new ThresholdTask(100);
        tt.setItem(img1);
        img = tt.call();
        // fill gaps along the considered axis
        DilationTask dt = new DilationTask();
        dt.setMask(MaskFactory.createFramedLine(12, alignment == Alignment.HORIZONTAL));
        dt.setItem(img);
        img = dt.call();
        // => detect smaller spots and convert to black
        DeleteSpotsTask dst = new DeleteSpotsTask(maxPixel / 5, Color.BLACK);
        dst.setItem(img);
        img = dst.call();
        // fill gaps between lines
        dt.setMask(MaskFactory.createFramedLine(5, alignment == Alignment.VERTICAL));
        dt.setItem(img);
        img = dt.call();
        // remove structures that are not elongated along the considered axis
        ErosionTask et = new ErosionTask();
        int n = alignment == Alignment.HORIZONTAL ? 3 : 13;
        int m = alignment == Alignment.HORIZONTAL ? 13 : 3;
        et.setMask(MaskFactory.createMaskNxM(n, m));
        et.setItem(img);
        img = et.call();
        // => detect smaller white areas and convert to black
        dst.setPixelThreshold(maxPixel);
        dst.setItem(img);
        img = dst.call();

        return openCloseSequence(img);
    }

    private Image openCloseSequence(Image bi) throws Exception {
        int[][] dm1, dm2;
        int n, m;

        if (alignment == Alignment.HORIZONTAL) {
            dm1 = new int[][]{{0, 0, 0, 1, 0, 0, 0}, {1, 1, 1, -1, 1, 1, 1}, {0, 0, 0, 1, 0, 0, 0}};
            dm2 = new int[][]{{1, 1, 1, -1, -1, -1, 1, 1, 1}};
            n = 3;
            m = 7;
        } else {
            n = 7;
            m = 3;
            dm1 = new int[][]{{0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {1, -1, 1}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}};
            dm2 = new int[][]{{1}, {1}, {1}, {-1}, {-1}, {-1}, {1}, {1}, {1}};
        }
        // elongate and dilate structures
        DilationTask dt = new DilationTask();
        dt.setMask(dm1);
        dt.setItem(bi);
        // fill bigger gaps vertical to considered axis
        bi = dt.call();
        dt.setMask(MaskFactory.createFramedLine(12, alignment == Alignment.VERTICAL));
        dt.setItem(bi);
        bi = dt.call();
        // elongate ones more
        dt.setMask(dm2);
        dt.setItem(bi);
        bi = dt.call();
        // => fill gaps in lines parallel to considered axis
        dt.setMask(MaskFactory.createFramedLine(15, alignment == Alignment.HORIZONTAL));
        dt.setItem(bi);
        bi = dt.call();
        // remove added pixels
        ErosionTask et = new ErosionTask();
        et.setItem(bi);
        et.setMask(MaskFactory.createMaskNxM(n, m));
        bi = et.call();
        return bi;
    }
}
