package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class ThresholdTask extends ACallable<Image, Image> {

    private double threshold = 5;

    private Color colorAboveThreshold = Color.WHITE;
    private Color colorBelowThreshold = Color.BLACK;

    public ThresholdTask() {
    }


    public ThresholdTask(double threshold) {
        this.threshold = threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public Color getColorAboveThreshold() {
        return colorAboveThreshold;
    }

    public void setColorAboveThreshold(Color colorAboveThreshold) {
        this.colorAboveThreshold = colorAboveThreshold;
    }

    public Color getColorBelowThreshold() {
        return colorBelowThreshold;
    }

    public void setColorBelowThreshold(Color colorBelowThreshold) {
        this.colorBelowThreshold = colorBelowThreshold;
    }

    /**
     * !!! overwrites image data !!!
     *
     * @return input image with new color data
     * @throws Exception
     */
    @Override
    public Image call() throws Exception {
        Color[][] imgData = getItem().getData();
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                if (imgData[x][y].getGreyValue() > threshold) {
                    imgData[x][y] = colorAboveThreshold;
                } else {
                    imgData[x][y] = colorBelowThreshold;
                }
            }
        }
        return getItem();
    }
}
