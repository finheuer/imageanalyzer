package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.util.Utils;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class ImageFilteringTask extends ACallable<Image, Image> {

    private double[][] filterMatrix = new double[][]{new double[]{-1, -1, -1}, new double[]{-1, 8, -1}, new double[]{-1, -1, -1}};

    public void setFilterMatrix(double[][] filterMatrix) {
        if (filterMatrix == null || filterMatrix[0] == null) {
            throw new NullPointerException();
        }
        if (filterMatrix.length % 2 == 0 || filterMatrix[0].length % 2 == 0) {
            throw new RuntimeException("Only odd sized matrices are allowed!");
        }
        this.filterMatrix = filterMatrix;
    }

    /**
     * @return new image object with new color array
     * @throws Exception
     */
    @Override
    public Image call() throws Exception {
        Color[][] imgData = getItem().getData();
        Color[][] newImgData = new Color[getItem().getWidth()][getItem().getHeight()];
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                LinkedList<ValueColor> colors = new LinkedList<>();
                int xOffset = filterMatrix[0].length / 2;
                int yOffset = filterMatrix.length / 2;
                for (int mx = -xOffset; mx <= xOffset; mx++) {
                    for (int my = -yOffset; my <= yOffset; my++) {
                        int[] pCorrected = getPointReplacement(x + mx, y + my);
                        //filterMatrix[my + offset][mx + offset] -> indices are switched because of logical correctness of filter matrix
                        //Images format is reversed indices of matrix indices
                        colors.add(new ValueColor(imgData[pCorrected[0]][pCorrected[1]], filterMatrix[my + yOffset][mx + xOffset]));
                        newImgData[x][y] = getMean(colors);
                    }
                }
            }
        }
        return new Image(newImgData);
    }

    private int[] getPointReplacement(int x, int y) {
        if (x < 0) {
            x = 0;
        }
        if (x >= getItem().getWidth()) {
            x = getItem().getWidth() - 1;
        }
        if (y < 0) {
            y = 0;
        }
        if (y >= getItem().getHeight()) {
            y = getItem().getHeight() - 1;
        }
        return new int[]{x, y};
    }


    private Color getMean(List<ValueColor> colors) {
        int r = 0, g = 0, b = 0;
        double cnt = 0;
        for (ValueColor c : colors) {
            cnt += c.getValue();
            r += c.getColor().getRed() * c.getValue();
            g += c.getColor().getGreen() * c.getValue();
            b += c.getColor().getBlue() * c.getValue();
        }
        cnt = cnt != 0 ? cnt : 1;
        return new Color(Utils.evalColor(r / cnt), Utils.evalColor(g / cnt), Utils.evalColor(b / cnt));
    }

    private class ValueColor {

        private Color color;
        private double value;

        ValueColor(Color color, double value) {
            this.color = color;
            this.value = value;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }

        double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

    }
}
