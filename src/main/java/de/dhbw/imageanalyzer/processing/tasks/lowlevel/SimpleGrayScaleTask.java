package de.dhbw.imageanalyzer.processing.tasks.lowlevel;

import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class SimpleGrayScaleTask extends ACallable<Image, Image> {

    /**
     * !!! overwrites color array
     *
     * @return input image with new color data
     */
    @Override
    public Image call() throws Exception {
        Color[][] imgData = getItem().getData();
        for (int x = 0; x < getItem().getWidth(); x++) {
            for (int y = 0; y < getItem().getHeight(); y++) {
                Color c = imgData[x][y];
                int gray = (int) ((c.getRed() + c.getBlue() + c.getGreen()) / 3.0);
                imgData[x][y] = new Color(gray, gray, gray);
            }
        }
        return getItem();
    }
}
