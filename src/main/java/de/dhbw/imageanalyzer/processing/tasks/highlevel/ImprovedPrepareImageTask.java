package de.dhbw.imageanalyzer.processing.tasks.highlevel;


import de.dhbw.imageanalyzer.data.Alignment;
import de.dhbw.imageanalyzer.data.Color;
import de.dhbw.imageanalyzer.data.Image;
import de.dhbw.imageanalyzer.processing.ACallable;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.*;
import de.dhbw.imageanalyzer.util.FilterFactory;
import de.dhbw.imageanalyzer.util.MaskFactory;

public class ImprovedPrepareImageTask extends ACallable<Image, Image> {

    private Alignment alignment;

    public ImprovedPrepareImageTask(Alignment alignment) {
        this.alignment = alignment;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    @Override
    public Image call() throws Exception {
        int maxPixel = getItem().getWidth() * getItem().getHeight() / 3000;

        // => smooth
//        ImageFilteringTask ift = FilterFactory.createGaussFilter(5, false).build();
        ImageFilteringTask ift = FilterFactory.createNxNFilter(5, 1).build();
        ift.setItem(getItem());
        Image img = ift.call();

        // => detect black lines on brighter background
        ift = FilterFactory.createThickLineDetectionFilter(alignment).build();
        ift.setItem(img);
        img = ift.call();

        CorrectEdgesTask cet = new CorrectEdgesTask(5);
        cet.setItem(img);
        img = cet.call();

        // => apply threshold: bright pixels to white, dark ones to black
        ThresholdTask tt = new ThresholdTask(img.getBrightnessPercentile(0.975));
        tt.setItem(img);
        img = tt.call();

        // fill gaps along the considered axis
        DilationTask dt = new DilationTask();
        dt.setMask(MaskFactory.createFramedLine(12, alignment == Alignment.HORIZONTAL));
        dt.setItem(img);
        img = dt.call();

        // detect smaller spots and convert to black
        DeleteSpotsTask dst = new DeleteSpotsTask(maxPixel, Color.BLACK);
        dst.setItem(img);
        img = dst.call();

        // fill gaps between lines
        dt.setMask(MaskFactory.createFramedLine(5, alignment == Alignment.VERTICAL));
        dt.setItem(img);
        img = dt.call();

        // remove big spots
        dst.setPixelThreshold(maxPixel * 2);
        dst.setItem(img);
        img = dst.call();

        // fill bigger gaps between lines
        dt.setMask(MaskFactory.createFramedLine(9, alignment == Alignment.VERTICAL));
        dt.setItem(img);
        img = dt.call();

        // remove edges
        ErosionTask et = new ErosionTask();
        et.setMask(MaskFactory.createSpotMask(3));
        et.setItem(img);
        img = et.call();

        // detect smaller white areas and convert to black
        dst.setPixelThreshold(maxPixel / 2);
        dst.setItem(img);
        img = dst.call();

        // add previously removed edges again
        dt.setMask(MaskFactory.createSpotMask(3));
        dt.setItem(img);
        img = dt.call();

        return img;
    }
}
