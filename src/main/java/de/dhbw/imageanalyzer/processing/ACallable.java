package de.dhbw.imageanalyzer.processing;

public abstract class ACallable<TAKES, RETURNS> {

    protected TAKES item;

    public ACallable() {
    }

    public ACallable(TAKES item) {
        this.item = item;
    }

    public void setItem(TAKES item) {
        this.item = item;
    }

    public TAKES getItem() {
        return item;
    }

    public abstract RETURNS call() throws Exception;
}
