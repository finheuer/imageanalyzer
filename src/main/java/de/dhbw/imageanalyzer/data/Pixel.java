package de.dhbw.imageanalyzer.data;

public class Pixel {

    private int x;
    private int y;

    private int r = Color.GREEN.getRed();
    private int g = Color.GREEN.getGreen();
    private int b = Color.GREEN.getBlue();

    public Pixel(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Pixel(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        setColor(color);
    }

    public int getX() {
        return x;
    }

    public Pixel setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public Pixel setY(int y) {
        this.y = y;
        return this;
    }

    public int getRed() {
        return r;
    }

    public Pixel setRed(int r) {
        this.r = r;
        return this;
    }

    public int getGreen() {
        return g;
    }

    public Pixel setGreen(int g) {
        this.g = g;
        return this;
    }

    public int getBlue() {
        return b;
    }

    public Pixel setBlue(int b) {
        this.b = b;
        return this;
    }

    public Color getColor()
    {
        return new Color(r, g, b);
    }

    public Pixel setColor(Color color)
    {
        setRed(color.getRed());
        setGreen(color.getGreen());
        setBlue(color.getBlue());
        return this;
    }

    @Override
    public int hashCode() {
        return x * 10000 + y;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Pixel)) {
            return false;
        }
        Pixel p = (Pixel) o;
        if (p.getX() == x && p.getY() == y) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "x:" + x + ", y:" + y;
    }


}
