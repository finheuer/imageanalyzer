package de.dhbw.imageanalyzer.data;

import de.dhbw.imageanalyzer.util.Utils;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Image /*implements Cloneable */ {

    private Color[][] data;
    private int width;
    private int height;

    public Image(Color[][] data) {
        this.data = data;
        calculateDimensions();
    }

    public Image(BufferedImage image) {
        this(Utils.getData(image));
    }

    public Color[][] getData() {
        return data;
    }

    public void setData(Color[][] data) {
        this.data = data;
        calculateDimensions();
    }

    private void calculateDimensions() {
        if (data != null && data.length > 0 && data[0].length > 0) {
            this.width = data.length;
            this.height = data[0].length;
        } else {
            throw new RuntimeException("Invalid dimensions!");
        }
    }

    public int getRGBA(int x, int y) {
        return data[x][y].getRGBA();
    }

    public void setRGBA(int x, int y, int color) {
        data[x][y] = new Color(color);
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    //    @Override
    public Image clone() {
        Color[][] newData = new Color[width][height];
        synchronized (data) {
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    newData[x][y] = data[x][y].clone();
                }
            }
        }
        return new Image(newData);
    }

    /**
     * this - toBeSubtracted (only colors; alpha gets mean value of both)
     *
     * @param toBeSubtracted image that will be subtracted from img1
     */
    public void subtract(Image toBeSubtracted) {

        Color[][] id2 = toBeSubtracted.getData();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int r = data[x][y].getRed() - id2[x][y].getRed() < 0 ? 0 : data[x][y].getRed() - id2[x][y].getRed();
                int g = data[x][y].getGreen() - id2[x][y].getGreen() < 0 ? 0 : data[x][y].getGreen() - id2[x][y].getGreen();
                int b = data[x][y].getBlue() - id2[x][y].getBlue() < 0 ? 0 : data[x][y].getBlue() - id2[x][y].getBlue();
                int alpha = (data[x][y].getAlpha() + id2[x][y].getAlpha()) / 2;
                setRGBA(x, y, Utils.calcRGB(r, g, b, alpha));
            }
        }
    }

    /**
     * Adds Color values only (alpha will be mean of both images)
     * this + img
     *
     * @param img second Image that will be added to this
     */
    public void addTo(Image img) {
        Color[][] id = img.getData();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int r = data[x][y].getRed() + id[x][y].getRed() > 255 ? 255 : data[x][y].getRed() + id[x][y].getRed();
                int g = data[x][y].getGreen() + id[x][y].getGreen() > 255 ? 255 : data[x][y].getGreen() + id[x][y].getGreen();
                int b = data[x][y].getBlue() + id[x][y].getBlue() > 255 ? 255 : data[x][y].getBlue() + id[x][y].getBlue();
                int alpha = (data[x][y].getAlpha() + id[x][y].getAlpha()) / 2;
                setRGBA(x, y, Utils.calcRGB(r, g, b, alpha));
            }
        }
    }

    /**
     * calculates mean/pixel of both images
     *
     * @param img second Image to be merged
     */
    public void mergeImage(Image img) {
        Color[][] id = img.getData();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int r = (data[x][y].getRed() + id[x][y].getRed()) / 2;
                int g = (data[x][y].getGreen() + id[x][y].getGreen()) / 2;
                int b = (data[x][y].getBlue() + id[x][y].getBlue()) / 2;
                int alpha = (data[x][y].getAlpha() + id[x][y].getAlpha()) / 2;
                setRGBA(x, y, Utils.calcRGB(r, g, b, alpha));
            }
        }
    }

    /**
     * negative values decrease contrast, whereas positive values increase contrast
     *
     * @param contrast double between -255 and 255
     */
    public void changeContrast(double contrast) {
        double factor = 259 * (contrast + 255) / (255 * (259 - contrast));
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int r = (int) factor * (data[x][y].getRed() - 128) + 128;
                int g = (int) factor * (data[x][y].getGreen() - 128) + 128;
                int b = (int) factor * (data[x][y].getBlue() - 128) + 128;

                setRGBA(x, y, Utils.calcRGB(Utils.evalColor(r), Utils.evalColor(g), Utils.evalColor(b), data[x][y].getAlpha()));
            }
        }
    }

    /**
     * @return average grey value of whole image
     */
    public double getMeanBrightness() {
        double brightness = 0;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                brightness += data[x][y].getGreyValue();
            }
        }
        return brightness / (width * height);
    }

    /**
     * the given percentage of pixels has a brightness below the returned value
     *
     * @param percentile between 0 and 1
     * @return brightness value
     */
    public int getBrightnessPercentile(double percentile) {
        double p = percentile <= 1 ? percentile : 1;
        p = p >= 0 ? p : 0;

        List<Color> test = new ArrayList<>();
        for (Color[] row : data) {
            test.addAll(Arrays.asList(row));
        }
        test.sort(Comparator.comparingInt(Color::getGreyValue));
        return test.get((int) (p * (test.size() - 1))).getGreyValue();
    }


}
