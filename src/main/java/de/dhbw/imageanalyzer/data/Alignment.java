package de.dhbw.imageanalyzer.data;

public enum Alignment {
    HORIZONTAL,
    VERTICAL,
    EAST,
    WEST,
    NORTH,
    SOUTH
}
