package de.dhbw.imageanalyzer.data;

public class TwoImages {

    private Image image1;
    private Image image2;

    public TwoImages(Image image1, Image image2) {
        this.image1 = image1;
        this.image2 = image2;
    }

    public Image getImage1() {
        return image1;
    }

    public Image getImage2() {
        return image2;
    }

    public void setImage1(Image image1) {
        this.image1 = image1;
    }

    public void setImage2(Image image2) {
        this.image2 = image2;
    }
}
