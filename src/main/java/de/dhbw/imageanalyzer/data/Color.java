package de.dhbw.imageanalyzer.data;

public class Color implements Cloneable {

    public static final Color WHITE = new Color(255, 255, 255);
    public static final Color BLACK = new Color(0, 0, 0);
    public static final Color GREEN = new Color(0, 255, 0);
    public static final Color RED = new Color(255, 0, 0);
    public static final Color BLUE = new Color(0, 0, 255);

    private int redVal;
    private int greenVal;
    private int blueVal;
    private int alpha;

    public Color() {
        redVal = 0;
        greenVal = 0;
        blueVal = 0;
        alpha = 255;
    }

    public Color(int color) {
        this.blueVal = color & 0xFF;
        this.greenVal = (color >> 8) & 0xFF;
        this.redVal = (color >> 16) & 0xFF;
        this.alpha = (color >> 24) & 0xFF;
    }

    public Color(java.awt.Color color) {
        this.redVal = color.getRed();
        this.greenVal = color.getGreen();
        this.blueVal = color.getBlue();
        this.alpha = color.getAlpha();
    }

    public Color(int redVal, int greenVal, int blueVal) {
        setRed(redVal);
        setGreen(greenVal);
        setBlue(blueVal);
        setAlpha(255);
    }

    public Color(int redVal, int greenVal, int blueVal, int alpha) {
        setRed(redVal);
        setGreen(greenVal);
        setBlue(blueVal);
        setAlpha(alpha);
    }

    public int getRed() {
        return redVal;
    }

    public void setRed(int red) {
        this.redVal = red & 0xFF;
    }

    public int getGreen() {
        return greenVal;
    }

    public void setGreen(int green) {
        this.greenVal = green & 0xFF;
    }

    public int getBlue() {
        return blueVal;
    }

    public void setBlue(int blue) {
        this.blueVal = blue & 0xFF;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha & 0xFF;
    }

    public int getGreyValue() {
        return (redVal + greenVal + blueVal) / 3;
    }

    public int getRGBA() {
        return (blueVal & 0xFF) | (greenVal << 8) | (redVal << 16) | (alpha << 24);
    }

    @Override
    public Color clone() {
        return new Color(getRGBA());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Color)) {
            return false;
        }
        Color c = (Color) o;
        return c.getRed() == redVal && c.getGreen() == greenVal && c.getBlue() == blueVal && c.getAlpha() == alpha;
    }
}
