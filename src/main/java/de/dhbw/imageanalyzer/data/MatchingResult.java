package de.dhbw.imageanalyzer.data;

public class MatchingResult {

    private final int totalKeyPoints;
    private final int totalKeyPoints1;
    private final int totalKeyPoints2;
    private final int matchingKeyPoints;
    private final int matchingKeyPointsInRange;
    private final double ratioOfKeyPointsInRange;
    private final double ratioOfMatches;
    private final double similarity;

    public MatchingResult(int totalKeyPoints1, int totalKeyPoints2, int matchingKeyPoints, int matchingKeyPointsInRange) {
        this.totalKeyPoints1 = totalKeyPoints1;
        this.totalKeyPoints2 = totalKeyPoints2;
        this.matchingKeyPoints = matchingKeyPoints;
        this.matchingKeyPointsInRange = matchingKeyPointsInRange;
        this.totalKeyPoints = totalKeyPoints1 > totalKeyPoints2 ? totalKeyPoints1 : totalKeyPoints2;
        this.ratioOfKeyPointsInRange = matchingKeyPointsInRange / ((double) matchingKeyPoints);
        this.ratioOfMatches = matchingKeyPoints / ((double) totalKeyPoints);
        this.similarity = ratioOfKeyPointsInRange;
    }

    public double getRatioOfKeyPointsInRange() {
        return ratioOfKeyPointsInRange;
    }

    public double getRatioOfMatches() {
        return ratioOfMatches;
    }

    public int getMatchingKeyPoints() {
        return matchingKeyPoints;
    }

    public int getMatchingKeyPointsInRange() {
        return matchingKeyPointsInRange;
    }

    public int getTotalKeyPoints1() {
        return totalKeyPoints1;
    }

    public int getTotalKeyPoints2() {
        return totalKeyPoints2;
    }

    public double getSimilarity() {
        return similarity;
    }
}
