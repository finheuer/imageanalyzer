package de.dhbw.imageanalyzer.data;

public class DebugAnalysisInfo {

    private final long totalTime;
    private final long preparingTime;
    private final long analyzingTime;

    public DebugAnalysisInfo(long totalTime, long preparingTime, long analyzingTime) {
        this.totalTime = totalTime;
        this.preparingTime = preparingTime;
        this.analyzingTime = analyzingTime;
    }

    /**
     * @return Analyzing time in ms
     */
    public long getAnalyzingTime() {
        return analyzingTime;
    }

    /**
     * @return Preparing time in ms
     */
    public long getPreparingTime() {
        return preparingTime;
    }

    /**
     * @return Total time in ms
     */
    public long getTotalTime() {
        return totalTime;
    }
}
