package de.dhbw.imageanalyzer.data;

import java.util.HashSet;
import java.util.Set;

public class Line {

    private HashSet<Pixel> pixels = new HashSet<>();
    private double meanWidth;
    private double length;
    private Alignment alignment;
    private Pixel topLeft;
    private Pixel topRight;
    private Pixel bottomLeft;
    private Pixel bottomRight;

    public Line(Set<Pixel> pixels, double meanWidth, double length, Alignment alignment, Pixel topLeft, Pixel topRight, Pixel bottomLeft, Pixel bottomRight) {
        this.pixels.addAll(pixels);
        this.meanWidth = meanWidth;
        this.length = length;
        this.alignment = alignment;
        this.topLeft = topLeft;
        this.topRight = topRight;
        this.bottomLeft = bottomLeft;
        this.bottomRight = bottomRight;
    }

    public Set<Pixel> getPixels() {
        return pixels;
    }

    public double getMeanWidth() {
        return meanWidth;
    }

    public Pixel getBottomLeft() {
        return bottomLeft;
    }

    public Pixel getBottomRight() {
        return bottomRight;
    }

    public Pixel getTopLeft() {
        return topLeft;
    }

    public Pixel getTopRight() {
        return topRight;
    }

    public double getLength() {
        return length;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public double getTotalCount() {
        return pixels.size();
    }

    public int getWidthOfRect() {
        int w = topRight.getX() - topLeft.getX();
        int h = bottomLeft.getY() - topLeft.getY();
        return Math.min(w, h);
    }
}
