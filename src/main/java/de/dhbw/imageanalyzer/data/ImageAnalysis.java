package de.dhbw.imageanalyzer.data;

import java.util.LinkedList;
import java.util.List;

public class ImageAnalysis {

    private Image originalImage;
    private List<Image> preparedImages;
    private LinkedList<Line> lines = new LinkedList<>();
    private LinkedList<Line> holes = new LinkedList<>();
    private DebugAnalysisInfo debugAnalysisInfo;

    public ImageAnalysis(Image originalImage) {
        this.originalImage = originalImage.clone();
    }

    public Image getOriginalImage() {
        return originalImage;
    }

    public void setPreparedImage(List<Image> preparedImages) {
        this.preparedImages = preparedImages;
    }

    public List<Image> getPreparedImage() {
        return preparedImages;
    }

    public void addLine(Line line) {
        lines.add(line);
    }

    public List<Line> getLines() {
        return lines;
    }

    public void addHole(Line hole) {
        holes.add(hole);
    }

    public List<Line> getHoles() {
        return holes;
    }

    public List<Line> getAllLineObjects() {
        List<Line> all = new LinkedList<>(lines);
        all.addAll(holes);
        return all;
    }

    public DebugAnalysisInfo getDebugAnalysisInfo() {
        return debugAnalysisInfo;
    }

    public void setDebugAnalysisInfo(DebugAnalysisInfo debugAnalysisInfo) {
        this.debugAnalysisInfo = debugAnalysisInfo;
    }
}
