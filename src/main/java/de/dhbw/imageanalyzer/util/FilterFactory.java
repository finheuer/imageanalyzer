package de.dhbw.imageanalyzer.util;


import de.dhbw.imageanalyzer.data.Alignment;
import de.dhbw.imageanalyzer.processing.tasks.lowlevel.ImageFilteringTask;

import java.util.Arrays;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class FilterFactory {

    public static HPFBuilder createHighPassFilter(int size) {
        return new HPFBuilder(size);
    }

    /**
     * @param size
     * @param unsharpMasking
     * @return
     */
    public static GFBuilder createGaussFilter(int size, boolean unsharpMasking) {
        return new GFBuilder(size, unsharpMasking);
    }

    public static NxNBuilder createNxNFilter(int size, double scale) {
        return new NxNBuilder().setScale(scale).setSize(size);
    }

    public static EFBuilder createEdgeFilter(Alignment alignment) {
        return new EFBuilder(alignment);
    }

    public static class VSFBuilder {

        private double value = 1.0;
        private double scaling = 2.0;

        private VSFBuilder() {
        }

        public VSFBuilder setValue(double value) {
            this.value = value;
            return this;
        }

        public VSFBuilder setScaling(double scaling) {
            this.scaling = scaling;
            return this;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask filter = new ImageFilteringTask();
            filter.setFilterMatrix(new double[][]{new double[]{-value, 0, value}, new double[]{-scaling * value, 0, scaling * value}, new double[]{-value, 0, value}});
            return filter;
        }
    }

    public static VSFBuilder createVerticalSobelFilter() {
        return new VSFBuilder();
    }

    public static class HSFBuilder {

        private double value = 1.0;
        private double scaling = 2.0;

        private HSFBuilder() {
        }

        public HSFBuilder setValue(double value) {
            this.value = value;
            return this;
        }

        public HSFBuilder setScaling(double scaling) {
            this.scaling = scaling;
            return this;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask filter = new ImageFilteringTask();
            filter.setFilterMatrix(new double[][]{new double[]{-value, -scaling * value, -value}, new double[]{0, 0, 0}, new double[]{value, scaling * value, value}});
            return filter;
        }
    }

    public static HSFBuilder createHorizontalSobelFilter() {
        return new HSFBuilder();
    }


    public static LDFBuilder createLineDetectionFilter(Alignment alignment) {
        return new LDFBuilder(alignment);
    }

    public static TLDFBuilder createThickLineDetectionFilter(Alignment alignment) {
        return new TLDFBuilder(alignment);
    }

    public static class HPFBuilder {

        private int size = 3;
        private double inner = 8.0;
        private double outer = -1.0;

        private HPFBuilder() {
        }

        private HPFBuilder(int size) {
            this.size = size;
        }

        public HPFBuilder setInner(double inner) {
            this.inner = inner;
            return this;
        }

        public HPFBuilder setOuter(double outer) {
            this.outer = outer;
            return this;
        }

        public HPFBuilder setSize(int size) {
            this.size = size;
            return this;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask filter = new ImageFilteringTask();
            double[][] kernel = new double[size][size];
            for (double[] row : kernel) {
                Arrays.fill(row, outer);
            }
            kernel[size / 2][size / 2] = inner;
            filter.setFilterMatrix(kernel);
            return filter;
        }
    }

    public static class EFBuilder {

        private double leftOrTop = 1.0;
        private double rightOrBottom = 1.0;
        private int size = 5;
        private Alignment alignment;

        private EFBuilder(Alignment alignment) {
            setAlignment(alignment);
        }

        public EFBuilder setAlignment(Alignment alignment) {
            if (alignment == Alignment.NORTH || alignment == Alignment.SOUTH) {
                alignment = Alignment.HORIZONTAL;
            }
            if (alignment == Alignment.EAST || alignment == Alignment.WEST) {
                alignment = Alignment.VERTICAL;
            }
            this.alignment = alignment;

            return this;
        }


        public EFBuilder setSize(int size) {
            this.size = size;
            return this;
        }

        public EFBuilder setRight(double right) {
            this.rightOrBottom = right;
            return this;
        }

        public EFBuilder setBottom(double bottom) {
            this.rightOrBottom = bottom;
            return this;
        }

        public EFBuilder setLeft(double left) {
            this.leftOrTop = left;
            return this;
        }

        public EFBuilder setTop(double top) {
            this.leftOrTop = top;
            return this;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask ift = new ImageFilteringTask();
            double[][] filter = new double[size][size];
            if (alignment == Alignment.HORIZONTAL) {
                int i = 0;
                for (double[] row : filter) {
                    if (i < size / 2) {
                        Arrays.fill(row, leftOrTop);
                    } else if (i == size / 2) {
                        Arrays.fill(row, 0);
                    } else {
                        Arrays.fill(row, -rightOrBottom);
                    }
                    i++;
                }
            } else {
                for (int c = 0; c < size; c++) {
                    for (int r = 0; r < size; r++) {
                        double fill = c == size / 2 ? 0 : c < size / 2 ? leftOrTop : -rightOrBottom;
                        filter[r][c] = fill;
                    }
                }
            }
            ift.setFilterMatrix(filter);
            return ift;

        }
    }

    public static class NxNBuilder {
        private double scale = 1.0;
        private int size = 3;

        public double getScale() {
            return scale;
        }

        private NxNBuilder setScale(double scale) {
            this.scale = scale;
            return this;
        }

        public int getSize() {
            return size;
        }

        public NxNBuilder setSize(int size) {
            this.size = size;
            return this;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask ift = new ImageFilteringTask();
            double[][] kernel = new double[size][size];

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    kernel[i][j] = scale;
                }
            }

            if (size % 2 == 0) {
                throw new RuntimeException("Only odd sized matrices are allowed!");
            }

            ift.setFilterMatrix(kernel);
            return ift;
        }
    }

    public static class LDFBuilder {
        private double outerVal = -1;
        private double innerVal = 2;
        private int sizeAlongAxis = 3;
        private int sizeVerticalToAxis = 3;
        private Alignment alignment;

        private LDFBuilder(Alignment alignment) {
            setAlignment(alignment);
        }

        public LDFBuilder setAlignment(Alignment alignment) {
            if (alignment == Alignment.NORTH || alignment == Alignment.SOUTH) {
                alignment = Alignment.HORIZONTAL;
            }
            if (alignment == Alignment.EAST || alignment == Alignment.WEST) {
                alignment = Alignment.VERTICAL;
            }
            this.alignment = alignment;
            return this;
        }

        public LDFBuilder setOuterValue(double outer) {
            this.outerVal = outer;
            return this;
        }

        public LDFBuilder setInnerValue(double inner) {
            innerVal = inner;
            return this;
        }

        public LDFBuilder setSize(int sizeAlongAxis, int sizeVerticalToAxis) {
            this.sizeAlongAxis = sizeAlongAxis;
            this.sizeVerticalToAxis = sizeVerticalToAxis;
            return this;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask ift = new ImageFilteringTask();
            double[][] kernel = alignment == Alignment.HORIZONTAL ? new double[sizeVerticalToAxis][sizeAlongAxis]
                    : new double[sizeAlongAxis][sizeVerticalToAxis];
            for (double[] row : kernel) {
                Arrays.fill(row, outerVal);
            }
            if (alignment == Alignment.HORIZONTAL) {
                Arrays.fill(kernel[sizeVerticalToAxis / 2], innerVal);
            } else {
                for (int i = 0; i < sizeAlongAxis; i++) {
                    kernel[i][sizeVerticalToAxis / 2] = innerVal;
                }
            }
            ift.setFilterMatrix(kernel);
            return ift;
        }
    }

    public static class GFBuilder {

        private int size = 3;

        private boolean unsharpMasking = false;

        private GFBuilder(int size, boolean unsharpMasking) {
            setSize(size);
            this.setUnsharpMasking(unsharpMasking);
        }

        public int getSize() {
            return size;
        }

        public GFBuilder setSize(int size) {
            if (size != 3 && size != 5) {
                throw new IndexOutOfBoundsException("Value must be 3 or 5!");
            }
            this.size = size;
            return this;
        }


        public boolean isUnsharpMasking() {
            return unsharpMasking;
        }

        public void setUnsharpMasking(boolean unsharpMasking) {
            this.unsharpMasking = unsharpMasking;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask ift = new ImageFilteringTask();
            double[][] kernel;
            if (size == 3) {
                kernel = new double[][]
                        {
                                new double[]{1.0, 2.0, 1.0},
                                new double[]{2.0, 4.0, 2.0},
                                new double[]{1.0, 2.0, 1.0},
                        };
            } else {
                kernel = new double[][]
                        {
                                new double[]{1.0, 4.0, 6.0, 4.0, 1.0},
                                new double[]{4.0, 16.0, 24.0, 16.0, 4.0},
                                new double[]{6.0, 24.0, 36.0, 24.0, 6.0},
                                new double[]{4.0, 16.0, 24.0, 16.0, 4.0},
                                new double[]{1.0, 4.0, 6.0, 4.0, 1.0},
                        };
                if (unsharpMasking) {
                    kernel[2][2] = -476;
                }

            }
            ift.setFilterMatrix(kernel);
            return ift;
        }
    }

    public static GradientBuilder createGradientBuilder(Alignment alignment) {
        return new GradientBuilder(alignment);
    }

    public static class GradientBuilder {

        private static final int SIZE = 3;

        private Alignment alignment = Alignment.NORTH;

        private GradientBuilder(Alignment alignment) {
            setAlignment(alignment);
        }

        public Alignment getAlignment() {
            return alignment;
        }

        public void setAlignment(Alignment alignment) {
            if (alignment == Alignment.HORIZONTAL) {
                alignment = Alignment.NORTH;
            }
            if (alignment == Alignment.VERTICAL) {
                alignment = Alignment.EAST;
            }
            this.alignment = alignment;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask ift = new ImageFilteringTask();
            double[][] kernel = new double[3][];

            switch (alignment) {
                case NORTH:
                    kernel = new double[][]
                            {
                                    new double[]{1.0, 1.0, 1.0},
                                    new double[]{1.0, -2.0, 1.0},
                                    new double[]{-1.0, -1.0, -1.0},
                            };
                    break;
                case SOUTH:
                    kernel = new double[][]
                            {
                                    new double[]{-1.0, -1.0, -1.0},
                                    new double[]{1.0, -2.0, 1.0},
                                    new double[]{1.0, 1.0, 1.0},
                            };
                    break;
                case EAST:
                    kernel = new double[][]
                            {
                                    new double[]{-1.0, 1.0, 1.0},
                                    new double[]{-1.0, -2.0, 1.0},
                                    new double[]{-1.0, 1.0, 1.0},
                            };
                    break;
                case WEST:
                    kernel = new double[][]
                            {
                                    new double[]{1.0, 1.0, -1.0},
                                    new double[]{1.0, -2.0, -1.0},
                                    new double[]{1.0, 1.0, -1.0},
                            };
                    break;
                default:
                    throw new IndexOutOfBoundsException("No suitable alignment found");
            }

            ift.setFilterMatrix(kernel);
            return ift;
        }

    }

    public static class TLDFBuilder {
        private Alignment alignment;
        private int length = 5;

        private TLDFBuilder(Alignment alignment) {
            this.alignment = alignment;
        }

        public TLDFBuilder setAlignment(Alignment alignment) {
            if (alignment == Alignment.NORTH || alignment == Alignment.SOUTH) {
                alignment = Alignment.HORIZONTAL;
            }
            if (alignment == Alignment.EAST || alignment == Alignment.WEST) {
                alignment = Alignment.VERTICAL;
            }
            this.alignment = alignment;
            return this;
        }

        public TLDFBuilder setLength(int length) {
            this.length = length;
            return this;
        }

        public ImageFilteringTask build() {
            ImageFilteringTask ift = new ImageFilteringTask();
            double[][] filter;
            if (alignment == Alignment.HORIZONTAL) {
                filter = new double[7][length];
                for (double[] row : filter) {
                    Arrays.fill(row, 1);
                }
                Arrays.fill(filter[3], -2);
                Arrays.fill(filter[2], -1);
                Arrays.fill(filter[4], -1);
            } else {
                filter = new double[length][7];
                for (double[] row : filter) {
                    Arrays.fill(row, 1);
                    row[2] = -1;
                    row[3] = -2;
                    row[4] = -1;
                }
            }
            ift.setFilterMatrix(filter);
            return ift;
        }
    }

}
