package de.dhbw.imageanalyzer.util;

import de.dhbw.imageanalyzer.processing.TaskScheduler;

import javax.swing.*;
import java.awt.*;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

/**
 * @author Fin Heuer <fin.heuer@dlr.de>.
 */
public class UIUtils {

    private static int CNT = 0;

    public static void showImage(de.dhbw.imageanalyzer.data.Image image) {
        showImage(Utils.getImage(image), "");
    }
    public static void showImage(de.dhbw.imageanalyzer.data.Image image, String title) {
        showImage(Utils.getImage(image), title);
    }

    public static void showImage(BufferedImage image) {
        showImage(image, "");
    }

    public static synchronized void showImage(BufferedImage image, String title) {
        CNT++;
        EventQueue.invokeLater(() -> {
            JFrame frame = new JFrame(title);
            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    CNT--;
                    if (CNT == 0) {
                        TaskScheduler.getInstance().shutDown();
                        System.exit(0);
                    }
                }
            });
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setSize(800, 600);
            JImagePanel panel = new JImagePanel(image);
            JScrollPane scrollPane = new JScrollPane(panel);
            panel.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
            frame.setContentPane(scrollPane);
            frame.setVisible(true);
        });
    }

    private static class JImagePanel extends JPanel {

        private final Image image;

        public JImagePanel(Image image) {
            this.image = image;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, this);
        }
    }
}
