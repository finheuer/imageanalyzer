package de.dhbw.imageanalyzer.util;

import de.dhbw.imageanalyzer.data.*;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * x == width == column
 * y == height == row
 *
 * @author Fin Heuer <fin.heuer@dlr.de>
 */
public class Utils {

    private Utils() {
    }

    public static BufferedImage getImage(Image image) {
        return getImage(image.getData());
    }

    public static BufferedImage getImage(Color[][] data) {
        if (data == null || data.length == 0 || data[0].length == 0) {
            return null;
        }
        int width = data.length;
        int height = data[0].length;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                image.setRGB(x, y, data[x][y].getRGBA());
        return image;
    }

    public static int calcRGB(int r, int g, int b, int alpha) {
        return b & 0xFF | (g << 8) | (r << 16) | (alpha << 24);
    }

    public static Color[] getAsColorArray(double[] array) {
        Color[] colors = new Color[array.length];
        for (int i = 0; i < array.length; i++) {
            colors[i] = new Color((int) array[i]);
        }
        return colors;
    }

    public static Color[][] getData(BufferedImage image) {
        Color[][] arr = new Color[image.getWidth()][image.getHeight()];
        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++)
                arr[x][y] = new Color(image.getRGB(x, y));
        return arr;
    }

    public static double[] getAs1DDoubleArray(Color[][] data) {
        if (data.length == 0 || data[0].length == 0) {
            return new double[]{};
        }
        double[] arr = new double[data.length * data[0].length];
        int cnt = 0;
        for (Color[] aData : data) {
            for (int y = 0; y < data[0].length; y++) {
                arr[cnt++] = aData[y].getRGBA();
            }
        }
        return arr;
    }

    public static Color[][] getAs2DIntArray(double[] data, int width, int height) {
        //TODO: Dimension checks!
        Color[][] arr = new Color[width][height];
        int x = 0;
        int y = 0;
        for (double aData : data) {
            arr[x][y] = new Color((int) aData);
            y++;
            if (y == height) {
                y = 0;
                x++;
            }
        }
        return arr;
    }

    public static Set<Pixel> countPixelsSearch(Color[][] img, int x, int y, Set<Pixel> checked) {
        LinkedList<Pixel> toCheck = new LinkedList<>();
        HashSet<Pixel> whitePixel = new HashSet<>();
        toCheck.add(new Pixel(x, y));
        while (!toCheck.isEmpty()) {
            Pixel check = toCheck.removeFirst();
            if (checked.contains(check)) {
                continue;
            }
            checked.add(check);
            if (img[check.getX()][check.getY()].equals(Color.WHITE) && !whitePixel.contains(check)) {
                whitePixel.add(check);
                for (int xi = -1; xi <= 1; xi++) {
                    for (int yi = -1; yi <= 1; yi++) {
                        if ((check.getX() + xi >= 0 && check.getX() + xi < img.length)
                                && (check.getY() + yi >= 0 && check.getY() + yi < img[0].length)
                                && !(xi == 0 && yi == 0)) {
                            Pixel p2 = new Pixel(check.getX() + xi, check.getY() + yi);
                            if (img[check.getX() + xi][check.getY() + yi].equals(Color.WHITE) && !checked.contains(p2)) {
                                toCheck.add(p2);
                            }
                        }
                    }
                }
            }
        }
        return whitePixel;
    }

    public static List<Set<Pixel>> getLinesInImage(Image image) {
        LinkedList<Set<Pixel>> lines = new LinkedList<>();
        HashSet<Pixel> checked = new HashSet<>();
        Color[][] pixels = image.getData();
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                lines.add(Utils.countPixelsSearch(pixels, x, y, checked));
            }
        }
        return lines;
    }

    public static List<Set<Pixel>> getLinesInImage(Image image, Color color) {
        List<Set<Pixel>> lines = getLinesInImage(image);
        for (Set<Pixel> set : lines) {
            for (Pixel pixel : set) {
                pixel.setColor(color);
            }
        }
        return lines;
    }

    public static List<Line> analyzeImage(List<Set<Pixel>> lines) {
        LinkedList<Line> analysedLines = new LinkedList<>();
        for (Set<Pixel> l : lines) {
            double meanWidth = 0.0, length = 0.0;
            if (l.isEmpty()) {
                continue;
            }
            int lowestX = Integer.MAX_VALUE, highestX = 0, lowestY = Integer.MAX_VALUE, highestY = 0;
            for (Pixel p : l) {
                if (p.getX() < lowestX) {
                    lowestX = p.getX();
                }
                if (p.getX() > highestX) {
                    highestX = p.getX();
                }
                if (p.getY() < lowestY) {
                    lowestY = p.getY();
                }
                if (p.getY() > highestY) {
                    highestY = p.getY();
                }
            }
            Pixel topLeft = new Pixel(lowestX, lowestY), topRight = new Pixel(highestX, lowestY),
                    bottomLeft = new Pixel(lowestX, highestY), bottomRight = new Pixel(highestX, highestY);
            int xLength = ((topRight.getX() + bottomRight.getX()) / 2) - ((topLeft.getX() + bottomLeft.getX()) / 2);
            int yLength = ((bottomLeft.getY() + bottomRight.getY()) / 2) - ((topRight.getY() + topLeft.getY()) / 2);
            Alignment alignment;
            if (xLength > yLength) {
                length = xLength;
                alignment = Alignment.HORIZONTAL;
                HashMap<Integer, Integer> pixelCounter = new HashMap<>();
                for (Pixel p : l) {
                    Integer i = pixelCounter.get(p.getX());
                    if (i == null) {
                        i = 0;
                    }
                    pixelCounter.put(p.getX(), ++i);
                }
                for (Integer i : pixelCounter.values()) {
                    meanWidth += i;
                }
                meanWidth = meanWidth / ((double) pixelCounter.size());
            } else {
                length = yLength;
                alignment = Alignment.VERTICAL;
                HashMap<Integer, Integer> pixelCounter = new HashMap<>();
                for (Pixel p : l) {
                    Integer i = pixelCounter.get(p.getY());
                    if (i == null) {
                        i = 0;
                    }
                    pixelCounter.put(p.getY(), ++i);
                }
                for (Integer i : pixelCounter.values()) {
                    meanWidth += i;
                }
                meanWidth = meanWidth / ((double) pixelCounter.size());
            }
            analysedLines.add(new Line(l, meanWidth, length, alignment, topLeft, topRight, bottomLeft, bottomRight));
        }
        return analysedLines;
    }

    public static BufferedImage visualizeResults(ImageAnalysis analysis) {
        if (analysis != null) {
            double alpha = 0.3;
            Color[][] data = insertAnalysis(analysis.getOriginalImage(), analysis).getData();
            for (Line line : analysis.getAllLineObjects()) {
                for (Pixel p : line.getPixels()) {
                    Color ci = data[p.getX()][p.getY()];
                    int r = (int) (p.getRed() * alpha + (1 - alpha) * ci.getRed());
                    int g = (int) (p.getGreen() * alpha + (1 - alpha) * ci.getGreen());
                    int b = (int) (p.getBlue() * alpha + (1 - alpha) * ci.getBlue());
                    data[p.getX()][p.getY()] = new Color(r, g, b);
                }
            }
            return getImage(data);
        }
        return null;
    }

    private static Image insertAnalysis(Image image, ImageAnalysis analysis) {
        Color[][] data = image.getData();
        for (Line line : analysis.getAllLineObjects()) {
            for (int x = line.getTopLeft().getX(); x < line.getTopRight().getX(); x++) {
                data[x][line.getTopLeft().getY()] = Color.RED;
                data[x][line.getBottomRight().getY()] = Color.RED;
            }
            for (int y = line.getTopLeft().getY(); y < line.getBottomLeft().getY(); y++) {
                data[line.getTopLeft().getX()][y] = Color.RED;
                data[line.getTopRight().getX()][y] = Color.RED;
            }
        }
        return image;
    }

    public static int evalColor(double value) {
        if (value < 0) {
            return 0;
        }
        if (value > 255) {
            return 255;
        }
        return (int) value;
    }

    public static Mat bufferedImageToMat(BufferedImage image) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", byteArrayOutputStream);
            byteArrayOutputStream.flush();
            return Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
        } catch (IOException e) {
            return null;
        }
    }

    public static BufferedImage matToBufferedImage(Mat mat) {
        try {
            MatOfByte mob = new MatOfByte();
            Imgcodecs.imencode(".jpg", mat, mob);
            return ImageIO.read(new ByteArrayInputStream(mob.toArray()));
        } catch (IOException e) {
            return null;
        }
    }

    public static Mat imageToMat(Image image) {
        BufferedImage img = getImage(image);
        return bufferedImageToMat(img);
    }

    public static Image matToImage(Mat mat) {
        BufferedImage bufferedImage = matToBufferedImage(mat);
        return new Image(bufferedImage);
    }
}
