package de.dhbw.imageanalyzer.util;

import java.util.Arrays;

/**
 * @author Ulrike Leipscher <ulrike.leipscher@dlr.de>.
 */
public class MaskFactory {

    /*
    patterns like:
    1 1 1
    1-1 1
    1 1 1

    1 1 1 1 1
    1 1-1 1 1
    1 1 1 1 1
     */
    public static int[][] createMaskNxM(int n, int m) {
        int[][] mask = new int[n][m];
        for (int[] row : mask) {
            Arrays.fill(row, 1);
        }
        mask[n / 2][m / 2] = -1;
        return mask;
    }

    /*
    patterns like:
    0   1   0
    1  -1   1
    0   1   0

    0 0 1 0 0
    0 1 1 1 0
    1 1-1 1 1
    0 1 1 1 0
    0 0 1 0 0
     */
    public static int[][] createSpotMask(int size) {
        int[][] mask = new int[size][size];
        int offset = size / 2;
        int rowN = -offset;
        for (int[] row : mask) {
            Arrays.fill(row, 0);
            for (int m = Math.abs(rowN) - offset; m <= offset - Math.abs(rowN); m++) {
                row[offset + m] = 1;
            }
            rowN++;
        }
        mask[offset][offset] = -1;
        return mask;
    }

    /*
    patterns like
    -1 1 1 1 1 1 -1

    -1
    1
    1
    1
    -1
     */
    public static int[][] createFramedLine(int n, boolean horizontal) {
        int[][] mask;
        if (horizontal) {
            mask = new int[1][n];
            Arrays.fill(mask[0], 1);
            mask[0][0] = -1;
            mask[0][n - 1] = -1;
        } else {
            mask = new int[n][1];
            for (int[] row : mask) {
                row[0] = 1;
            }
            mask[0][0] = -1;
            mask[n - 1][0] = -1;
        }
        return mask;
    }
}
